from pathlib import Path
import os
import subprocess
from itertools import chain
from typing import List, Iterable


def multi_glob(parent: Path, pattern_list: List[str], *, recursive=False) -> Iterable[Path]:
    if not recursive:
        func_glob = lambda s: parent.glob(s)
    else:
        func_glob = lambda s: parent.rglob(s)
        
    return chain(*(map(func_glob, pattern_list)))


# ===========================================================

here = os.path.dirname(__file__)
here = Path(here)

# 找到.c, .h, .cpp, .hpp, .cxx, .hxx
file_list = multi_glob(here, [ '*.[ch]', '*.[ch]pp', '*.[ch]xx' ], recursive=True)

file_path_list = map(lambda f: str(f.relative_to(here)), file_list)
text = '\n'.join(file_path_list)

list_file = here / 'temp-clang-format-source-files.txt'
list_file.write_text(text)

print('== SOURCE FILE LIST ==')
print(text)
print('======================')

input('PROCEED? ')

subprocess.call(f'clang-format -i -files="{list_file.name}"', shell=True)

input('EXIT→')

os.remove(list_file)
